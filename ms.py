import random

class Character:
    def __init__(self, name, health, attack, defense):
        self.name = name
        self.health = health
        self.attack = attack
        self.defense = defense

    def take_damage(self, damage):
        self.health -= damage

    def is_alive(self):
        return self.health > 0

    def attack_enemy(self, enemy):
        damage = self.attack - enemy.defense
        if damage > 0:
            enemy.take_damage(damage)

class Enemy(Character):
    def __init__(self, name, health, attack, defense):
        super().__init__(name, health, attack, defense)

class Player(Character):
    def __init__(self, name):
        super().__init__(name, 100, 20, 10)
        self.inventory = []

    def display_inventory(self):
        print("Inventory:")
        for item in self.inventory:
            print(item)

    def pick_up_item(self, item):
        self.inventory.append(item)

    def drop_item(self, item):
        if item in self.inventory:
            self.inventory.remove(item)

def combat(player, enemy):
    while player.is_alive() and enemy.is_alive():
        player.attack_enemy(enemy)
        if enemy.is_alive():
            enemy.attack_enemy(player)

        print(f"{player.name}: {player.health} HP | {enemy.name}: {enemy.health} HP")

    if player.is_alive():
        print(f"{player.name} defeated {enemy.name}!")
    else:
        print(f"{player.name} was defeated by {enemy.name}. Game over!")

def main():
    player_name = input("Enter your character's name: ")
    player = Player(player_name)

    enemies = [Enemy("Goblin", 50, 10, 5), Enemy("Dragon", 200, 30, 20), Enemy("Witch", 80, 15, 10)]

    print("Welcome to the Text RPG Game!")

    while player.is_alive():
        enemy = random.choice(enemies)
        print(f"A wild {enemy.name} appears!")
        action = input("Do you want to [F]ight or [R]un? ").lower()

        if action == 'f':
            combat(player, enemy)
            if player.is_alive():
                loot = f"{enemy.name}'s loot"
                player.pick_up_item(loot)
                print(f"You obtained {loot}.")
        elif action == 'r':
            print(f"{player.name} runs away from the {enemy.name}.")
        else:
            print("Invalid choice. Choose 'F' to fight or 'R' to run.")

    print("Game over. Thanks for playing!")

if __name__ == "__main":
    main()
